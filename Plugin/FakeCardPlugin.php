<?php
/**
 * Created by PhpStorm.
 * User: nickolay
 * Date: 19.11.15
 * Time: 10:51
 */

namespace a3mg\FakePaymentBundle\Plugin;

use JMS\Payment\CoreBundle\Model\ExtendedDataInterface;
use JMS\Payment\CoreBundle\Model\FinancialTransactionInterface;
use JMS\Payment\CoreBundle\Model\PaymentInstructionInterface;
use JMS\Payment\CoreBundle\Plugin\AbstractPlugin;
use JMS\Payment\CoreBundle\Plugin\Exception\Action\VisitUrl;
use JMS\Payment\CoreBundle\Plugin\Exception\ActionRequiredException;
use JMS\Payment\CoreBundle\Plugin\Exception\BlockedException;
use JMS\Payment\CoreBundle\Plugin\Exception\Exception;
use JMS\Payment\CoreBundle\Plugin\Exception\FinancialException;
use JMS\Payment\CoreBundle\Plugin\Exception\PaymentPendingException;
use JMS\Payment\CoreBundle\Plugin\Exception\TimeoutException;
use JMS\Payment\CoreBundle\Plugin\PluginInterface;
use JMS\Payment\CoreBundle\Plugin\ErrorBuilder;
use Symfony\Bundle\FrameworkBundle\Routing\Router;

class FakeCardPlugin extends AbstractPlugin
{
    /** @var  Router */
    protected $router;

    /**
     * FakePlugin constructor.
     * @param $router
     */
    public function __construct($router)
    {
        $this->router = $router;
    }

    public function checkPaymentInstruction(PaymentInstructionInterface $instruction)
    {
        $errorBuilder = new ErrorBuilder();
        $data = $instruction->getExtendedData();

        if (!$data->get('holder')) {
            $errorBuilder->addDataError('holder', 'form.error.required');
        }
        if (!$data->get('number')) {
            $errorBuilder->addDataError('number', 'form.error.required');
        }

        if ($instruction->getAmount() > 10000) {
            $errorBuilder->addGlobalError('form.error.credit_card_max_limit_exceeded');
        }

        // more checks here ...

        if ($errorBuilder->hasErrors()) {
            throw $errorBuilder->getException();
        }
    }

    public function approveAndDeposit(FinancialTransactionInterface $transaction, $retry)
    {
        /** @var PaymentInstructionInterface $instruction */
        $instruction = $transaction->getPayment()->getPaymentInstruction();

        $data = $instruction->getExtendedData();

        if (!$data->get('success')) {
            $exception = new Exception();
            $exception->setFinancialTransaction($transaction);
            throw $exception;
        }
        if ($data->get('blocked')) {
            $blocked = new BlockedException();
            $blocked->setFinancialTransaction($transaction);
            throw $blocked;
        }
        if ($data->get('timeout')) {
            $timeout = new TimeoutException();
            $timeout->setFinancialTransaction($transaction);
            throw $timeout;
        }
        if ($data->get('actionRequired')) {
            $actionRequest = new ActionRequiredException('Confirm payment');
            $actionRequest->setFinancialTransaction($transaction);
            $actionRequest->setAction(new VisitUrl($this->router->generate('a3mg_fake_payment_action_required', array(
                'transaction' => $transaction->getId()
            ))));
            throw $actionRequest;
        }

        $transaction->setProcessedAmount($instruction->getAmount());
        $transaction->setResponseCode(PluginInterface::RESPONSE_CODE_SUCCESS);
        $transaction->setReasonCode(PluginInterface::REASON_CODE_SUCCESS);
    }

    public function processes($name)
    {
        return 'fake_credit_card' === $name;
    }
}