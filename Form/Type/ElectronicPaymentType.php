<?php
/**
 * Created by PhpStorm.
 * User: nickolay
 * Date: 19.11.15
 * Time: 10:59
 */

namespace a3mg\FakePaymentBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class ElectronicPaymentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', 'email', array('required' => false))

            ->add('success', 'checkbox', array('required' => false, 'value' => true))
            ->add('blocked', 'checkbox', array('required' => false, 'value' => false))
            ->add('timeout', 'checkbox', array('required' => false, 'value' => false))
            ->add('actionRequired', 'checkbox', array('required' => false, 'value' => false))
        ;
    }

    public function getName()
    {
        return 'fake_electronic_payment';
    }
}