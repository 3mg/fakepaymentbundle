<?php
/**
 * Created by PhpStorm.
 * User: nickolay
 * Date: 19.11.15
 * Time: 11:07
 */

namespace a3mg\FakePaymentBundle\Controller;

use JMS\Payment\CoreBundle\Model\FinancialTransactionInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class FakePaymentController extends Controller
{
    public function actionRequiredAction(Request $request) {
        $transactionId = $request->query->get("transaction");
        if (!$transactionId) {
            return new Response('Bad request', Response::HTTP_BAD_REQUEST);
        }

        /** @var $em \Doctrine\ORM\EntityManager */
        $em = $this->container->get("doctrine")->getManager();
        $transaction = $em->find("JMSPaymentCoreBundle:FinancialTransaction", $transactionId);

        if (!$transaction instanceof FinancialTransactionInterface) {
            return new Response('Transaction not found', Response::HTTP_NOT_FOUND);
        }

        if (Request::METHOD_GET == $request->getMethod()) {
            return $this->render("FakePaymentBundle:FakePayment:action_required.html.twig", array(

            ));
        } else if (Request::METHOD_POST == $request->getMethod()) {
            $parameters = [
                'OutSum' => $transaction->getRequestedAmount(),
                'InvId' => $transaction->getPayment()->getPaymentInstruction()->getId(),
            ];

            if ($request->request->get("confirm") == 1) {
                return $this->redirectToRoute("a3mg_fake_payment_success", $parameters);
            } else if ($request->request->get("cancel") == 1) {
                return $this->redirectToRoute("a3mg_fake_payment_fail", $parameters);
            } else {
                return new Response('Bad request', Response::HTTP_BAD_REQUEST);
            }
        } else {
            return new Response('Method not allowed', Response::HTTP_METHOD_NOT_ALLOWED);
        }
    }

    public function callbackAction(Request $request)
    {
        $out_sum = $request->get('OutSum');
        $inv_id = $request->get('InvId');
        $sign = $request->get('SignatureValue');

        $instruction = $this->getInstruction($inv_id);
        /** @var FinancialTransactionInterface $transaction */
        if (null === $transaction = $instruction->getPendingTransaction()) {
            return new Response('FAIL', 500);
        }
        try {
            if ($instruction->getPaymentSystemName() == "fake_card") {
                $this->get('payment.plugin.fake_card')->approveAndDeposit($transaction->getPayment()->getId(), $out_sum);
            } else if ($instruction->getPaymentSystemName() == "fake") {
                $this->get('payment.plugin.fake')->approveAndDeposit($transaction->getPayment()->getId(), $out_sum);
            }
        } catch (\Exception $e) {
            return new Response('FAIL', 500);
        }
        $this->getDoctrine()->getManager()->flush();
        return new Response('OK' . $inv_id);
    }

    public function successAction(Request $request)
    {
        $out_sum = $request->get('OutSum');
        $inv_id = $request->get('InvId');
        $sign = $request->get('SignatureValue');


        $instruction = $this->getInstruction($inv_id);
        $data = $instruction->getExtendedData();
        return $this->redirect($data->get('return_url'));
    }

    public function failAction(Request $request)
    {
        $inv_id = $request->get('InvId');
        $instruction = $this->getInstruction($inv_id);
        $data = $instruction->getExtendedData();
        return $this->redirect($data->get('cancel_url'));
    }

    private function getInstruction($id)
    {
        $instruction = $this->getDoctrine()->getManager()->getRepository('JMSPaymentCoreBundle:PaymentInstruction')->find($id);
        if (empty($instruction)) {
            throw new \Exception('Cannot find instruction id='.$id);
        }
        return $instruction;
    }
}